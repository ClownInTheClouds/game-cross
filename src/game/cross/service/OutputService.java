package game.cross.service;

public interface OutputService {

    void printField(char[][] gameField);

    void printPlayerVictory();

    void printAiVictory();

    void printDraw();
}
