package game.cross.service;

import java.util.Map;

public interface InputService {

    Map.Entry<Integer, Integer> getCoordinate(char[][] gameField);
}
