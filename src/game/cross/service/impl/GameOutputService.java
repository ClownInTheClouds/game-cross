package game.cross.service.impl;

import game.cross.service.OutputService;

import static game.cross.constant.OutputConstant.*;
import static java.lang.System.lineSeparator;

public class GameOutputService implements OutputService {

    @Override
    public void printField(char[][] gameField) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(FIELD_BORDER_UP).append(lineSeparator());
        for (char[] row : gameField) {
            stringBuilder.append(CELL_BORDER_LEFT);
            for (char symbol : row) {
                stringBuilder.append(symbol).append(CELL_BORDER_RIGHT);
            }
            stringBuilder.append(lineSeparator());
        }
        stringBuilder.append(FIELD_BORDER_DOWN);
        System.out.println(stringBuilder);
    }

    @Override
    public void printPlayerVictory() {
        System.out.println(PLAYER_VICTORY_MESSAGE);
    }

    @Override
    public void printAiVictory() {
        System.out.println(AI_VICTORY_MESSAGE);
    }

    @Override
    public void printDraw() {
        System.out.println(DRAW_MESSAGE);
    }
}
