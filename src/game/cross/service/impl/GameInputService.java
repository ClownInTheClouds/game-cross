package game.cross.service.impl;

import game.cross.service.InputService;
import game.cross.service.Validator;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Scanner;

import static game.cross.constant.InputConstant.ENTRE_COORDINATES_MESSAGE;

public class GameInputService implements InputService {

    private final Scanner scanner = new Scanner(System.in);
    private final Validator validator;

    public GameInputService(Validator validator) {
        this.validator = validator;
    }

    @Override
    public Map.Entry<Integer, Integer> getCoordinate(char[][] gameField) {
        int x, y;
        do {
            System.out.println(ENTRE_COORDINATES_MESSAGE);
            String input = scanner.nextLine();
            String[] coordinates = input.split(" ");
            x = Integer.parseInt(coordinates[0]) - 1;
            y = Integer.parseInt(coordinates[1]) - 1;
        } while (!validator.checkCellValid(gameField, x, y));
        return new AbstractMap.SimpleEntry<>(x, y);
    }
}
