package game.cross.service.impl;

import game.cross.service.AlternativeIntelligent;
import game.cross.service.Validator;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;

import static game.cross.constant.GameSettings.*;

public class GameAI implements AlternativeIntelligent {

    private final Random random = new Random();
    private final Validator validator;

    public GameAI(Validator validator) {
        this.validator = validator;
    }

    @Override
    public Map.Entry<Integer, Integer> guessNextMove(char[][] gameField) {
        char[][] copyOfGameField = Arrays.stream(gameField)
                .map(char[]::clone)
                .toArray(char[][]::new);
        for (int x = 0; x < GAME_FIELD_SIZE_X; x++) {
            for (int y = 0; y < GAME_FIELD_SIZE_Y; y++) {
                if (!validator.checkCellValid(gameField, x, y)) continue;
                char prevChar = copyOfGameField[x][y];
                copyOfGameField[x][y] = AI_DOT;
                if (validator.checkVictory(copyOfGameField, AI_DOT)) {
                    return new AbstractMap.SimpleEntry<>(x, y);
                }
                copyOfGameField[x][y] = PLAYER_DOT;
                if (validator.checkVictory(copyOfGameField, PLAYER_DOT)) {
                    return new AbstractMap.SimpleEntry<>(x, y);
                }
                copyOfGameField[x][y] = prevChar;
            }
        }
        return makeRandomMove(gameField);
    }

    @Override
    public Map.Entry<Integer, Integer> makeRandomMove(char[][] gameField) {
        int x, y;
        do {
            x = random.nextInt(GAME_FIELD_SIZE_X);
            y = random.nextInt(GAME_FIELD_SIZE_Y);
        } while (!validator.checkCellValid(gameField, x, y));
        return new AbstractMap.SimpleEntry<>(x, y);
    }
}
