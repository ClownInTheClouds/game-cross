package game.cross.service.impl;

import game.cross.service.Validator;

import static game.cross.constant.GameSettings.*;

public class GameValidator implements Validator {

    @Override
    public boolean checkVictory(char[][] gameField, char symbol) {
        return checkRows(gameField, symbol) ||              // Проверяем по строкам
               checkColumns(gameField, symbol) ||           // Проверяем по столбцам
               checkDiagonals(gameField, symbol) ||         // Проверяем по диагоналям
               checkReverseDiagonals(gameField, symbol);    // Проверяем по обратным диагоналям
    }

    @Override
    public boolean checkGameFieldFull(char[][] gameField) {
        for (char[] row : gameField)
            for (char symbol : row)
                if (symbol == EMPTY_DOT) return false;
        return true;
    }

    @Override
    public boolean checkCellValid(char[][] gameField, int x, int y) {
        if (x < 0 || y < 0 || x > GAME_FIELD_SIZE_X - 1 || y > GAME_FIELD_SIZE_Y - 1) return false;
        return (gameField[x][y] == EMPTY_DOT);
    }

    private boolean checkRows(char[][] gameField, char sym) {
        for (char[] row : gameField)
            if (sequenceOfRepeatingCharacters(row, sym)) return true;
        return false;
    }

    private boolean checkColumns(char[][] gameField, char sym) {
        for (int y = 0; y < GAME_FIELD_SIZE_Y; y++) {
            char[] column = new char[GAME_FIELD_SIZE_X];
            for (int x = 0; x < GAME_FIELD_SIZE_X; x++) {
                column[x] = gameField[x][y];
            }
            if (sequenceOfRepeatingCharacters(column, sym)) return true;
        }
        return false;
    }

    private boolean checkDiagonals(char[][] gameField, char sym) {
        int minSize = Math.min(GAME_FIELD_SIZE_X, GAME_FIELD_SIZE_Y);
        for (int i = 0; i < minSize; i++) {
            final int diagonalLength = Math.min(i + 1, minSize);
            char[] upperDiagonal = new char[diagonalLength];
            char[] lowerDiagonal = new char[diagonalLength];
            for (int j = 0; j < diagonalLength; j++) {
                upperDiagonal[j] = gameField[j][GAME_FIELD_SIZE_Y - diagonalLength + j];
                lowerDiagonal[j] = gameField[GAME_FIELD_SIZE_X - diagonalLength + j][j];
            }
            if (sequenceOfRepeatingCharacters(upperDiagonal, sym) ||
                sequenceOfRepeatingCharacters(lowerDiagonal, sym)
            ) return true;
        }
        return false;
    }

    private boolean checkReverseDiagonals(char[][] gameField, char sym) {
        int minSize = Math.min(GAME_FIELD_SIZE_X, GAME_FIELD_SIZE_Y);
        for (int i = 0; i < minSize; i++) {
            final int diagonalLength = Math.min(i + 1, minSize);
            char[] upperReverseDiagonal = new char[diagonalLength];
            char[] lowerReverseDiagonal = new char[diagonalLength];
            for (int j = 0; j < diagonalLength; j++) {
                upperReverseDiagonal[j] = gameField[i - j][j];
                lowerReverseDiagonal[j] = gameField[GAME_FIELD_SIZE_X - 1 - j][GAME_FIELD_SIZE_Y - diagonalLength + j];
            }
            if (sequenceOfRepeatingCharacters(upperReverseDiagonal, sym) ||
                sequenceOfRepeatingCharacters(lowerReverseDiagonal, sym)
            ) return true;
        }
        return false;
    }

    // Метод проверки массива на наличие последовательности одинаковых символов определенной длины
    private boolean sequenceOfRepeatingCharacters(char[] array, char searchChar) {
        int cnt = 1;
        char prevChar = array[0];
        for (int i = 1; i < array.length; i++) {
            char curChar = array[i];
            if (prevChar != searchChar) {
                if (curChar == searchChar) {
                    prevChar = curChar;
                    cnt = 1;
                }
            } else {
                if (prevChar == curChar) {
                    if (++cnt >= VICTORY_SEQUENCE_LENGTH) return true;
                } else {
                    cnt = 0;
                }
            }
        }
        return false;
    }
}
