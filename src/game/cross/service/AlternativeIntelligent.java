package game.cross.service;

import java.util.Map;

public interface AlternativeIntelligent {

    Map.Entry<Integer, Integer> guessNextMove(char[][] gameField);

    Map.Entry<Integer, Integer> makeRandomMove(char[][] gameField);
}
