package game.cross.service;

public interface Validator {

    boolean checkVictory(char[][] gameField, char symbol);

    boolean checkGameFieldFull(char[][] gameField);

    boolean checkCellValid(char[][] gameField, int x, int y);
}
