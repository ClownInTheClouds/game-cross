package game.cross;

import game.cross.service.AlternativeIntelligent;
import game.cross.service.InputService;
import game.cross.service.OutputService;
import game.cross.service.Validator;
import game.cross.service.impl.GameAI;
import game.cross.service.impl.GameInputService;
import game.cross.service.impl.GameOutputService;
import game.cross.service.impl.GameValidator;

import java.util.*;

import static game.cross.constant.GameSettings.*;

public class GameCross {

    private static final char[][] FIELD = new char[GAME_FIELD_SIZE_X][GAME_FIELD_SIZE_Y];

    private static final Validator VALIDATOR = new GameValidator();
    private static final InputService INPUT = new GameInputService(VALIDATOR);
    private static final OutputService OUTPUT = new GameOutputService();
    private static final AlternativeIntelligent AI = new GameAI(VALIDATOR);

    public static void main(String[] args) {
        initField();
        for (boolean playersTurn = true; !isGameOver(); playersTurn = !playersTurn) {
            if (playersTurn) playerStep();
            else aiStep();
        }
    }

    private static boolean isGameOver() {
        OUTPUT.printField(FIELD);
        if (VALIDATOR.checkVictory(FIELD, PLAYER_DOT)) {
            OUTPUT.printPlayerVictory();
            return true;
        }
        if (VALIDATOR.checkVictory(FIELD, AI_DOT)) {
            OUTPUT.printAiVictory();
            return true;
        }
        if (VALIDATOR.checkGameFieldFull(FIELD)) {
            OUTPUT.printDraw();
            return true;
        }
        return false;
    }

    private static void initField() {
        for (int i = 0; i < GAME_FIELD_SIZE_X; i++)
            for (int j = 0; j < GAME_FIELD_SIZE_Y; j++)
                FIELD[i][j] = EMPTY_DOT;
    }

    private static void playerStep() {
        Map.Entry<Integer, Integer> coordinate = INPUT.getCoordinate(FIELD);
        FIELD[coordinate.getKey()][coordinate.getValue()] = PLAYER_DOT;
    }

    private static void aiStep() {
        Map.Entry<Integer, Integer> nextMove = AI.guessNextMove(FIELD);
        FIELD[nextMove.getKey()][nextMove.getValue()] = AI_DOT;
    }

}