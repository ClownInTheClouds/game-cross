package game.cross.constant;

public final class OutputConstant {

    public static final String FIELD_BORDER_UP = "-------";
    public static final String FIELD_BORDER_DOWN = "-------";
    public static final String CELL_BORDER_LEFT = "|";
    public static final String CELL_BORDER_RIGHT = "|";

    public static final String PLAYER_VICTORY_MESSAGE = "Player WIN!";
    public static final String AI_VICTORY_MESSAGE = "Win SkyNet!";
    public static final String DRAW_MESSAGE = "DRAW";

    private OutputConstant() {}
}
