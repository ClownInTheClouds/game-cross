package game.cross.constant;

public final class InputConstant {

    public static final String ENTRE_COORDINATES_MESSAGE = "Введите координаты: X Y";

    private InputConstant() {}
}
