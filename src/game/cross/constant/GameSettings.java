package game.cross.constant;

public final class GameSettings {

    public static final int GAME_FIELD_SIZE_X = 5;
    public static final int GAME_FIELD_SIZE_Y = 5;
    public static final int VICTORY_SEQUENCE_LENGTH = 4;

    public static final char PLAYER_DOT = 'X';
    public static final char AI_DOT = '0';
    public static final char EMPTY_DOT = '.';

    private GameSettings() {}
}
